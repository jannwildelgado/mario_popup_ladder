import Vue from 'vue'
import Router from 'vue-router'

// = Pages
import main from './views/main'

Vue.use(Router)

// = routes
let routes = [
  {
		path: '/',
		name: 'main-page',
		component: main
	}
]

const router = new Router({
	mode: 'history',
	routes
})

export default router